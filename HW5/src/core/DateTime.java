package core;

public class DateTime 
{
	int year,month,day;
	int hour,minute,second;
	
	public DateTime (String a,String b)
	{
		String[] date = a.split("-");
		year = Integer.parseInt(date[0]);
		month = Integer.parseInt(date[1]);
		day = Integer.parseInt(date[2]);
		
		String[] time = b.split(":");
		hour = Integer.parseInt(time[0]);
		minute = Integer.parseInt(time[1]);
		second = Integer.parseInt(time[2]);
	}
	
	public DateTime (int date[],int time[])
	{
		year = date[0];
		month = date[1];
		day = date[2];
		
		hour = time[0];
		minute = time[1];
		second = time[2];
	}
	
	public String getDate ()
	{
		return year+"-"+month+"-"+day;
	}
	
	public String getHour ()
	{
		return hour+":"+minute+":"+second;
	}
	
	public int[] getDateInt()
	{
		int[] aux = {0,0,0};
		aux[0] = year; 
		aux[1] = month;
		aux[2] = day;
		return aux;
	}
	
	public int[] getHourInt()
	{
		int[] aux = {0,0,0};
		aux[0] = hour; 
		aux[1] = minute;
		aux[2] = second;
		return aux;
	}
	
	public String toString()
	{
		return year+"-"+month+"-"+day+"/"+hour+":"+minute+":"+second;
	}
}
