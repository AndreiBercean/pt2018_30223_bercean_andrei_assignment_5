package core;

import java.io.*;
import java.nio.file.*;
import java.util.*;
import java.util.stream.*;

public class Process 
{
	static List<MonitoredData> load(String path)
	{
		String fileName = path;
		
		List<String> list = new ArrayList<>();
		List<MonitoredData> dataList = new ArrayList<>();
		
		try (Stream<String> stream = Files.lines(Paths.get(fileName))) 
		{
			list = stream
					.collect(Collectors.toList());
		}
		catch (IOException e) 
		{
			e.printStackTrace();
		}

		for(String s : list)
		{
			dataList.add(new MonitoredData(s));
		}
		return dataList;
	}
	
	static void activityMap(List<MonitoredData> dataList)
	{
		Map<String, Long> aux = dataList.stream().collect(
                Collectors.groupingBy(MonitoredData::getActivity, Collectors.counting()));
		
		aux = aux.entrySet().stream()
			.sorted(Map.Entry.comparingByValue())
			.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue,
					(oldValue, newValue) -> oldValue, HashMap::new));
		
		try (PrintWriter pw = new PrintWriter("activityMap.txt", "UTF-8")) {
			aux.entrySet().stream()
		          .forEach(pw::println);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
	}
	
	static void dayCount(List<MonitoredData> dataList)
	{
		Map<String, Long> dayCount = dataList.stream().collect(
                Collectors.groupingBy(MonitoredData::getStartTimeDate, Collectors.counting()));
		Map<String, Long> dayCount2 = dataList.stream().collect(
                Collectors.groupingBy(MonitoredData::getEndTimeDate, Collectors.counting()));
		int max;
		if(dayCount.size() > dayCount2.size()) max = dayCount.size();
		else max = dayCount2.size();
		System.out.println(max);
	}
	
	static void activityDayCount(List<MonitoredData> dataList)
	{
		Map<String, DateTime> aux = dataList.stream()
										.map(temp->{
											int date[] = {0,0,0};
											DateTime f = new DateTime(date,temp.durationToInt()); 
											return new java.util.AbstractMap.SimpleEntry<String, DateTime>(temp.getActivity(),f);
												})
										.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue,
												(oldValue, newValue) -> oldValue, HashMap::new));
		
		try (PrintWriter pw = new PrintWriter("activityDuration.txt", "UTF-8")) {
			aux.entrySet().stream()
		          .forEach(pw::println);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
	}
	
	public static void main(String args[]) 
	{
		List<MonitoredData> dataList = load("C:\\Users\\Asus F3Eseries\\Desktop\\HW5\\Activities.txt");
		
		activityMap(dataList);
		dayCount(dataList);
		activityDayCount(dataList);
		
	}
}
