package core;

public class MonitoredData 
{
	private DateTime startTime;
	private DateTime endTime;
	private String activity = new String("");
	
	public MonitoredData(String s)
	{
		String[] input = s.split("[ \t]");
		startTime = new DateTime(input[0],input[1]);
		endTime = new DateTime(input[3],input[4]);
		activity = input[6];
	}
	
	public String toString()
	{
		return startTime.toString()+"\n"+endTime.toString()+"\n"+activity;
	}

	public DateTime getStartTime() {
		return startTime;
	}
	
	public String getStartTimeDate() {
		return startTime.getDate();
	}
	
	public String getStartTimeHour() {
		return startTime.getHour();
	}

	public DateTime getEndTime() {
		return endTime;
	}
	
	public String getEndTimeDate() {
		return endTime.getDate();
	}
	
	public String getEndTimeHour() {
		return endTime.getHour();
	}

	public String getActivity() {
		return activity;
	}
	
	public String durationToString()
	{
		int start[] = startTime.getHourInt();
		int end[] = endTime.getHourInt();
		int hour = start[0] - end[0], min = start[1] - end[1], sec = start[2] - end[2];
		return hour+":"+min+":"+sec;
	}
	
	public int[] durationToInt()
	{
		int start[] = startTime.getHourInt();
		int end[] = endTime.getHourInt();
		int time[] = {0, 0, 0};
		time[0] = end[0] - start[0];
		time[1] = end[1] - start[1];
		time[2] = end[2] - start[2];
		while(time[2]<0)
		{
			time[1]-=1;
			time[2] += 60;
		}
		while(time[1]<0)
		{
			time[0]-=1;
			time[1] += 60;
		}
		return time;
	}
}
